package com.crescent.weathermvvm.repository

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.crescent.weathermvvm.api.ApiClient
import com.crescent.weathermvvm.model.WeatherResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailsFragmentRepository (val application: Application){
    val showProgress = MutableLiveData<Boolean>()
    val response = MutableLiveData<WeatherResponse>()
    var lastRequestTime : Long = -1

    fun getWeather(woeid: Int ){

        //Handling configuration changes
        if((System.currentTimeMillis() - lastRequestTime) < 10000){
            return
        }
        showProgress.value = true

        val call: Call<WeatherResponse> = ApiClient.getClient.getWeather(woeid)
        call.enqueue(object : Callback<WeatherResponse> {
            override fun onFailure(call: Call<WeatherResponse>?, t: Throwable?) {
                showProgress.value= false
                Toast.makeText(application, "Error wile accessing the API", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<WeatherResponse>?,
                resp: Response<WeatherResponse>?
            ) =
                if (resp!!.isSuccessful) {
                   lastRequestTime = System.currentTimeMillis()
                    response.value = resp.body()
                    showProgress.value = false

                } else {
                    Toast.makeText(application, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

}