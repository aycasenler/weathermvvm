package com.crescent.weathermvvm.repository

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.crescent.weathermvvm.api.ApiClient
import com.crescent.weathermvvm.api.ApiInterface

import com.crescent.weathermvvm.model.Location
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class SearchFragmentRepository(val application: Application) {
    val showProgress = MutableLiveData<Boolean>()
    val locationList = MutableLiveData<List<Location>>()

    fun changeState() {
        showProgress.value = !(showProgress.value != null && showProgress.value!!)
    }

    fun searchLocation(location: String) {
        showProgress.value = true

        val call: Call<List<Location>> = ApiClient.getClient.getLocation(location)
        call.enqueue(object : Callback<List<Location>> {
            override fun onFailure(call: Call<List<Location>>?, t: Throwable?) {
                showProgress.value = false
                Toast.makeText(application, "Error wile accessing the API", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<List<Location>>?,
                response: Response<List<Location>>?
            ) =
                if (response!!.isSuccessful) {
                    locationList.value = response.body()
                showProgress.value = false

                } else {
                    Toast.makeText(application, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }


}