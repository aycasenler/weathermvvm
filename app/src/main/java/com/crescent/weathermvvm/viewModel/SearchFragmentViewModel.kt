package com.crescent.weathermvvm.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.crescent.weathermvvm.model.Location
import com.crescent.weathermvvm.repository.SearchFragmentRepository

class SearchFragmentViewModel (application: Application): AndroidViewModel(application){

    private val repository = SearchFragmentRepository(application)
    val showProgress: LiveData<Boolean>
    val locationList : LiveData<List<Location>>

    init {
        showProgress = repository.showProgress
        locationList = repository.locationList
    }

    fun changeState(){
        repository.changeState()
    }
    fun searchLocation(location: String){
        repository.searchLocation(location)
    }
}