package com.crescent.weathermvvm.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.crescent.weathermvvm.model.WeatherResponse
import com.crescent.weathermvvm.repository.DetailsFragmentRepository

class DetailsFragmentViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = DetailsFragmentRepository(application)
    val showProgress: LiveData<Boolean>
    val response: LiveData<WeatherResponse>

    init {
        showProgress = repository.showProgress
        response = repository.response
    }

    fun getWeather(woeId : Int){
        repository.getWeather(woeId)
    }
}