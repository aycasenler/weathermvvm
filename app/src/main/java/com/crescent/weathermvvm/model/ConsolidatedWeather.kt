package com.crescent.weathermvvm.model

data class ConsolidatedWeather(
    val air_pressure: Double,
    val humidity: Int,
    val id: Long,
    val max_temp: Double,
    val min_temp: Double,
    val the_temp: Double,
    val wind_direction: Double,
    val wind_speed: Double
)