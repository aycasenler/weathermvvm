package com.crescent.weathermvvm.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.crescent.weathermvvm.R
import com.crescent.weathermvvm.adapter.LocationRvAdapter
import com.crescent.weathermvvm.viewModel.SearchFragmentViewModel
import kotlinx.android.synthetic.main.search_fragment.view.*

class SearchFragment : Fragment() {
    private lateinit var viewModel: SearchFragmentViewModel
    private lateinit var adapter: LocationRvAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.search_fragment, container, false)
        viewModel = ViewModelProvider(this).get(SearchFragmentViewModel::class.java)

        view.search_iv.setOnClickListener {
            if (view.search_et.text!!.isNotEmpty())
                viewModel.searchLocation(view.search_et.text.toString())
        }

        viewModel.showProgress.observe(viewLifecycleOwner, Observer {
            if (it)
                view.search_progress.visibility = VISIBLE
            else
                view.search_progress.visibility = GONE
        })

        viewModel.locationList.observe(viewLifecycleOwner, Observer {
            adapter.setLocationList(it)
        })

        adapter = LocationRvAdapter(requireContext())
        view.search_rv.adapter = adapter

        return view
    }
}