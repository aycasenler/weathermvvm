package com.crescent.weathermvvm.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.crescent.weathermvvm.R
import com.crescent.weathermvvm.viewModel.DetailsFragmentViewModel
import kotlinx.android.synthetic.main.details_fragment.view.*

class DetailsFragment :Fragment(){
    private lateinit var viewModel: DetailsFragmentViewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.details_fragment, container, false)

        viewModel = ViewModelProvider(this).get(DetailsFragmentViewModel::class.java)

        val location = this.arguments?.getInt("location")
        val name = this.arguments?.getString("name")

        view.location_tv.text = name
        viewModel.getWeather(location!!)

        viewModel.showProgress.observe(viewLifecycleOwner, Observer {
            if(it)
                view.details_progress.visibility = VISIBLE
            else
                view.details_progress.visibility = GONE
        })

        viewModel.response.observe(viewLifecycleOwner, Observer {
            if(it != null){
                view.temp_tv.text = it.consolidated_weather[0].the_temp.toString()
                view.max_temp_tv.text = it.consolidated_weather[0].max_temp.toString()
                view.min_temp_tv.text = it.consolidated_weather[0].min_temp.toString()
                view.humidity_tv.text = it.consolidated_weather[0].humidity.toString()
                view.wind_speed_tv.text = it.consolidated_weather[0].wind_speed.toString()
                view.air_pressure_tv.text = it.consolidated_weather[0].air_pressure.toString()
            }


        })
        return view
    }

}