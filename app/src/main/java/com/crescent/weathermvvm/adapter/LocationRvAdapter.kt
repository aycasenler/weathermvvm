package com.crescent.weathermvvm.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.crescent.weathermvvm.R
import com.crescent.weathermvvm.model.Location
import kotlinx.android.synthetic.main.rv_line_design.view.*

class LocationRvAdapter ( private val context: Context) :
    RecyclerView.Adapter<LocationRvAdapter.ViewHolder>() {
    private var list: List<Location> = ArrayList()

    fun setLocationList(list: List<Location>){
        this.list = list
        notifyDataSetChanged()
    }
    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = list[position].title
        holder.latLng.text = list[position].latt_long
        holder.itemView.setOnClickListener {
            clickLocation(position,it)

        }
    }
    private fun clickLocation(position: Int, view: View) {
        val bundle = Bundle()
        bundle.putInt("location",list[position].woeid)
        bundle.putString("name",list[position].title)
        Navigation.findNavController(view).navigate(R.id.detailsFragment, bundle)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.rv_line_design,
                parent,
                false
            )
        )
    }


    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val name = v.location_name_tv!!
        val latLng = v.lat_lng_tv!!
    }

}